package com.epam;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

/**
 * Unit test for simple App.
 */
@RunWith(Parameterized.class)
public class AppTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }

    private int[] inputArray;
    private int[] expectedSortedArray;

    public AppTest(int[] inputArray, int[] expectedSortedArray) {
        this.inputArray = inputArray;
        this.expectedSortedArray = expectedSortedArray;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{5, 4, 7, 6, 3, 2, 1, 10}, new int[]{1, 2, 3, 4, 5, 6, 7, 10}},
                {new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}},
                {new int[]{1, 2, 3, 4, 5}, new int[]{1, 2, 3, 4, 5}},
                {new int[]{1}, new int[]{1}},  // Edge case: empty array
                {new int[]{100, 50, 75, 25}, new int[]{25, 50, 75, 100}},
                // Add more test cases as needed
        });
    }

    @org.junit.Test
    public void testSorting() {
        Sort.sortArray(inputArray);
        assertArrayEquals(expectedSortedArray, inputArray);
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void testSortingAppWithMoreThanTenArguments() {
        int[] args = { 5, 2, 8, 1, 3, 7, 4, 9, 6, 10, 11 };
        Sort.sortArray(args);

        // Assuming SortingApp handles more than 10 arguments gracefully
        assertArrayEquals(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }, args);
    }
}
