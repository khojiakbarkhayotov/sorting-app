package com.epam;

public class Sort {
    public static void sortArray(int[] unsorted){
        if(unsorted.length == 0 || unsorted.length > 10){
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < unsorted.length - 1; i++) {
            for (int j = 0; j < unsorted.length - i - 1; j++) {
                if(unsorted[j] > unsorted[j + 1]){
                    int temp = unsorted[j];
                    unsorted[j] = unsorted[j+1];
                    unsorted[j+1] = temp;
                }
            }
        }
    }
}
