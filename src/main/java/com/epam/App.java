package com.epam;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Hello world!
 *
 */
@Data
@Getter
@Setter
public class App 
{
    private int array_length = 0;
    public static void main( String[] args )
    {
        int[] array = HandleInput.get();
        Sort.sortArray(array);
        Logger.output(array);
    }
}
