package com.epam;

public class Logger {
    public static void output(int[] sortedArray){
        System.out.println("Your sorted array is :");
        for (int element:
             sortedArray) {
            System.out.println(element);
        }
    }
}
