package com.epam;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Logger;

public class HandleInput {
    public static int[] get(){
        int[] res = new int[10];
        Scanner in = new Scanner(System.in);
        System.out.println("Input the random list of 10 integer values:");
        for (int i = 0; i < 10; i++) {
            try{
                int obtainedValue = in.nextInt();
                res[i] = obtainedValue;
            }catch (IndexOutOfBoundsException e){
                Logger.getLogger(e.toString());
            }
        }
        return res;
    }
}
